from cpu.state import State8080
from instructions.registry import load_instructions

from struct import unpack
from instructions.registry import INSTRUCTIONS
import sys


def conditioncodes_as_bit(state):
    bit = 0 | state.condition_codes.s
    bit <<= 1
    bit |= state.condition_codes.z
    bit <<= 2
    bit |= state.condition_codes.ac
    bit <<= 2
    bit |= state.condition_codes.p
    bit <<= 1
    bit |= 1
    bit <<= 1
    bit |= state.condition_codes.cy

    return bit


def emulate(state, i):
    opcode = state.memory[state.pc]

    try:
        instr = INSTRUCTIONS[opcode]
        print('opcode {}'.format(hex(opcode)))
        print ('a:{:04x} bc:{:04x} de:{:04x} hl:{:04x} pc:{:04x} sp{:04x}, #:{}'.format(
            state.a << 8 | conditioncodes_as_bit(state),
            state.b << 8 | state.c,
            state.d << 8 | state.e,
            state.h << 8 | state.l,
            state.pc,
            state.sp,
            i
        ))
        instr.run()
    except KeyError:
        print('Instruction {} not implemented'.format(hex(opcode)))
        sys.exit()


def read_to_memory_at(state, file, address):
    with open(file, 'rb') as f:
        content = f.read()
        length = len(content)
        content = unpack('B' * length, content)
        for i in range(length):
            state.memory[address + i] = content[i]


if __name__ == "__main__":
    load_instructions()
    state = State8080()

    read_to_memory_at(state, 'invaders.h', 0)
    read_to_memory_at(state, 'invaders.g', 0x800)
    read_to_memory_at(state, 'invaders.f', 0x1000)
    read_to_memory_at(state, 'invaders.e', 0x1800)

    i = 0
    while True:
        emulate(state, i)
        i += 1
