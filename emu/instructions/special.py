from instructions.base import Instruction
from instructions.registry import register


@register()
class NOP(Instruction):
    opcode = 0x00

    def _run(self):
        pass


@register()
class OUT(Instruction):
    opcode = 0xd3

    def _run(self):
        # implement functionality later
        self.state.pc += 1


@register()
class EI(Instruction):
    opcode = 0xfb

    def _run(self):
        self.state.interrupt_enabled = True
