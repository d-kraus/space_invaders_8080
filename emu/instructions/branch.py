from instructions.base import Instruction
from instructions.registry import register


@register()
class JNZ_Addr(Instruction):
    opcode = 0xc2

    def run(self):
        if not self.state.condition_codes.z:
            self.state.pc = self.state.get_addr_from_opcodes()
        else:
            self.state.pc += 3


@register()
class JMP_Addr(Instruction):
    opcode = 0xc3

    def run(self):
        self.state.pc = self.state.get_addr_from_opcodes()


@register()
class RET(Instruction):
    opcode = 0xc9

    def run(self):
        self.state.pc = (self.state.get_from_stack(1) << 8) |\
            self.state.get_from_stack()
        self.state.sp += 2


@register()
class CALL(Instruction):
    opcode = 0xcd

    def run(self):
        return_addr = self.state.pc + 2
        self.state.memory[self.state.sp - 1] = (return_addr >> 8) & 0xff
        self.state.memory[self.state.sp - 2] = return_addr & 0xff
        self.state.sp -= 2
        self.state.pc = self.state.get_addr_from_opcodes()
