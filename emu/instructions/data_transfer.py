from instructions.base import Instruction
from instructions.registry import register


@register()
class LXI_B(Instruction):
    opcode = 0x01

    def _run(self):
        self.state.c = self.state.get_opcode(1)
        self.state.b = self.state.get_opcode(2)
        self.state.pc += 2


@register()
class MVI_B(Instruction):
    opcode = 0x06

    def _run(self):
        self.state.b = self.state.get_opcode(1)
        self.state.pc += 1


@register()
class MVI_C(Instruction):
    opcode = 0x0e

    def _run(self):
        self.state.c = self.state.get_opcode(1)
        self.state.pc += 1


@register()
class LXI_D(Instruction):
    opcode = 0x11

    def _run(self):
        self.state.e = self.state.get_opcode(1)
        self.state.d = self.state.get_opcode(2)
        self.state.pc += 2


@register()
class LDAX_D(Instruction):
    opcode = 0x1a

    def _run(self):
        offset = (self.state.d << 8) | self.state.e
        self.state.a = self.state.memory[offset]


@register()
class LXI_H(Instruction):
    opcode = 0x21

    def _run(self):
        self.state.l = self.state.get_opcode(1)
        self.state.h = self.state.get_opcode(2)
        self.state.pc += 2


@register()
class MVI_H(Instruction):
    opcode = 0x26

    def _run(self):
        self.state.h = self.state.get_opcode(1)
        self.state.pc += 1


@register()
class LXI_SP(Instruction):
    opcode = 0x31

    def _run(self):
        self.state.sp = self.state.get_addr_from_opcodes()
        self.state.pc += 2


@register()
class STA_addr(Instruction):
    opcode = 0x32

    def _run(self):
        addr = self.state.get_addr_from_opcodes()
        self.state.memory[addr] = self.state.a
        self.state.pc += 2


# MVI M,D8    2       (HL) <- byte 2
@register()
class MVI_M(Instruction):
    opcode = 0x36

    def _run(self):
        addr = (self.state.h << 8) | self.state.l
        self.state.memory[addr] = self.state.get_opcode(1)
        self.state.pc += 1


@register()
class LDA_addr(Instruction):
    opcode = 0x3a

    def _run(self):
        addr = self.state.get_addr_from_opcodes()
        self.state.a = self.state.memory[addr]
        self.state.pc += 2


@register()
class MVI_A(Instruction):
    opcode = 0x3e

    def _run(self):
        self.state.a = self.state.get_opcode(1)
        self.state.pc += 1


@register()
class MOV_L_A(Instruction):
    opcode = 0x6f

    def _run(self):
        self.state.l = self.state.a


@register()
class MOV_M_A(Instruction):
    opcode = 0x77

    def _run(self):
        offset = (self.state.h << 8) | self.state.l
        self.state.memory[offset] = self.state.a


@register()
class MCV_A_H(Instruction):
    opcode = 0x7c

    def _run(self):
        self.state.a = self.state.h


@register()
class XCHG(Instruction):
    opcode = 0xeb

    def _run(self):
        h = self.state.h
        l = self.state.l
        self.state.h = self.state.d
        self.state.l = self.state.e
        self.state.d = h
        self.state.e = l
