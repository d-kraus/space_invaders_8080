from instructions.base import Instruction
from instructions.utils import parity_lookup
from instructions.registry import register


class LogicMixIn(object):
    def update_logic_flags(self):
        self.state.condition_codes.cy = False
        self.state.condition_codes.ac = False
        self.state.condition_codes.z = self.state.a == 0
        self.state.condition_codes.s = 0x80 == (self.state.a & 0x80)
        self.state.condition_codes.p = parity_lookup[self.state.a]


class LogicalInstruction(Instruction, LogicMixIn):
    def run(self):
        super().run()
        self.update_logic_flags()


@register()
class ANA_A(LogicalInstruction):
    opcode = 0xa7

    def _run(self):
        self.state.a = self.state.a & self.state.a


@register()
class XRA_A(LogicalInstruction):
    opcode = 0xaf

    def _run(self):
        self.state.a = self.state.a ^ self.state.a


@register()
class CPI_Data(Instruction):
    opcode = 0xfe

    def _run(self):
        result = self.state.a - self.state.get_opcode(1)

        self.state.condition_codes.z = result == 0
        self.state.condition_codes.s = (result & 0x80) == 0x80
        self.state.condition_codes.p = parity_lookup[result & 0xff]
        self.state.condition_codes.ac = (result & 0xff) > 0xf
        self.state.condition_codes.cy = result < self.state.get_opcode(1)
        self.state.pc += 1


class ANI_B(LogicalInstruction):
    opcode = 0xe6

    def run(self, state):
        result = state.a & self.get_opcode_at_offset(1)
        state.condition_codes.z = result == 0
        state.condition_codes.s = 0x80 == (result & 0x80)
        state.condition_codes.cy = 0  # Data book states ANI clears CY
        state.condition_codes.p = parity_lookup[result]
        state.a = result


class CMA(LogicalInstruction):
    opcode = 0x2F

    def run(self, state):
        state.a = ~state.a


class RRC(LogicalInstruction):
    opcode = 0x0f

    def run(self, state):
        value = state.a
        state.a = ((value & 1)) << 7 | (value >> 1)
        state.condition_codes.cy = 1 == value & 1


class RAR(LogicalInstruction):
    opcode = 0x1f

    def run(self, state):
        value = state.a
        state.a = state.condition_codes.cy << 7 | (value >> 1)
        state.condition_codes.cy = 1 == value & 1


class CPI(LogicalInstruction):
    opcode = 0xfe

    def run(self, state):

        result = state.a - self.get_opcode_at_offset(1)
        state.condition_codes.z = result == 0
        state.condition_codes.s = 0x80 == (result & 0x80)
        state.condition_codes.cy = state.a < self.get_opcode_at_offset(1)
        state.condition_codes.p = parity_lookup[result]
        state.pc += 1


class STC(LogicalInstruction):
    opcode = 0x37

    def run(self, state):
        state.condition_codes.cy = True


class CMC(LogicalInstruction):
    opcode = 0x3f

    def run(self, state):
        state.condition_codes.cy = ~state.condition_codes.cy
