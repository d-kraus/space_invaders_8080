import importlib

INSTRUCTIONS = {}


def register(*args):
    def decorator(cls):
        INSTRUCTIONS[cls.opcode] = cls()
        return cls
    return decorator


def load_instructions():
    files = [
        'arithmetic',
        'branch',
        'logical',
        'stack',
        'data_transfer',
        'special',
    ]

    for file in files:
        importlib.import_module('instructions.' + file)
