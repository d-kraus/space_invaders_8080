from cpu.state import State8080


class Instruction(object):

    def __init__(self):
        self.state = State8080()

    def _run(self):
        raise NotImplementedError

    def run(self):
        self._run()
        self.state.pc += 1
