from instructions.base import Instruction
from instructions.registry import register


@register()
class POP_B(Instruction):
    opcode = 0xc1

    def _run(self):
        self.state.c = self.state.memory[self.state.sp]
        self.state.b = self.state.memory[self.state.sp + 1]
        self.state.sp += 2


@register()
class Push_B(Instruction):
    opcode = 0xc5

    def _run(self):
        self.state.memory[self.state.sp - 1] = self.state.b
        self.state.memory[self.state.sp - 2] = self.state.c
        self.state.sp -= 2


@register()
class POP_D(Instruction):
    opcode = 0xd1

    def _run(self):
        self.state.d = self.state.memory[self.state.sp]
        self.state.e = self.state.memory[self.state.sp + 1]
        self.state.sp += 2


@register()
class Push_D(Instruction):
    opcode = 0xd5

    def _run(self):
        self.state.memory[self.state.sp - 1] = self.state.d
        self.state.memory[self.state.sp - 2] = self.state.e
        self.state.sp -= 2


@register()
class POP_H(Instruction):
    opcode = 0xe1

    def _run(self):
        self.state.l = self.state.memory[self.state.sp]
        self.state.h = self.state.memory[self.state.sp + 1]
        self.state.sp += 2


@register()
class Push_H(Instruction):
    opcode = 0xe5

    def _run(self):
        self.state.memory[self.state.sp - 1] = self.state.h
        self.state.memory[self.state.sp - 2] = self.state.l
        self.state.sp -= 2


class POP_PSW(Instruction):
    opcode = 0xf1

    def run(self, state):
        state.a = state.memor[state.sp + 1]
        psw = state.memory[state.sp]
        state.condition_codes.z = 0x01 == psw & 0x01
        state.condition_codes.s = 0x02 == psw & 0x02
        state.condition_codes.p = 0x04 == psw & 0x04
        state.condition_codes.cy = 0x05 == psw & 0x05
        state.condition_codes.ac = 0x10 == psw & 0x10
        state.sp += 2


class PUSH_PSW(Instruction):
    opcode = 0xf5

    def run(self, state):
        state.memory[state.sp - 1] = state.a
        psw = state.condition_codes.z |\
            state.condition_codes.s << 1 |\
            state.condition_codes.p << 2 |\
            state.condition_codes.cy << 3 |\
            state.condition_codes.ac << 4
        state.memory[state.sp - 2] = psw
        state.sp -= 2
