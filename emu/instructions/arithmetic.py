from instructions.base import Instruction
from instructions.utils import parity_lookup
from instructions.registry import register


class ArithmeticInstruction(Instruction):
    def update_condition_codes(self, result):
        self.sate.condition_codes.z = result & 0xff == 0
        self.sate.condition_codes.s = result & 0x80 == 0x80
        self.sate.condition_codes.cy = result > 0xff
        self.sate.condition_codes.p = parity_lookup[result & 0xff]

    def _run(self):
        raise NotImplementedError

    def run(self):
        result = self._run()
        self.update_condition_codes(result)
        # self.state.a = result & 0xff
        self.state.pc += 1


class Z_S_P_AC_MixIn(object):
    def update_condition_codes(self, result):
        self.state.condition_codes.z = result == 0
        self.state.condition_codes.s = (result & 0x80) == 0x80
        self.state.condition_codes.p = parity_lookup[result & 0xff]
        self.state.condition_codes.ac = (result & 0xff) > 0xf


@register()
class DCR_B(Instruction, Z_S_P_AC_MixIn):
    opcode = 0x05

    def _run(self):
        result = self.state.b - 1
        self.update_condition_codes(result)
        self.state.b = result & 0xff


@register()
class DAD_B(Instruction):
    opcode = 0x09

    def _run(self):
        result = ((self.state.h << 8) | self.state.l) +\
            ((self.state.b << 8) | self.state.c)

        self.state.h = result >> 8
        self.state.l = result & 0xff
        self.state.condition_codes.cy = result > 0xffff0000


@register()
class DECR_C(Instruction):
    opcode = 0x0d

    def _run(self):
        result = self.state.c - 1
        self.state.condition_codes.z = result == 0
        self.state.condition_codes.s = (result & 0x80) == 0x80
        self.state.condition_codes.p = parity_lookup[result & 0xff]
        self.state.condition_codes.ac = (result & 0xff) > 0xf
        self.state.c = result & 0xff


@register()
class INX_D(Instruction):
    opcode = 0x13

    def _run(self):
        # note: No condiditon pairs are affected
        value = ((self.state.d << 8) | self.state.e) + 1
        self.state.d = value >> 8
        self.state.e = value & 0xff


@register()
class INR_D(Instruction, Z_S_P_AC_MixIn):
    opcode = 0x14

    def _run(self):
        result = self.state.d + 1
        self.update_condition_codes(result)
        self.state.d = result & 0xff


@register()
class DAD_D(Instruction):
    opcode = 0x19

    def _run(self):
        result = ((self.state.h << 8) | self.state.l) +\
            ((self.state.d << 8) | self.state.e)

        self.state.h = result >> 8
        self.state.l = result & 0xff
        self.state.condition_codes.cy = result > 0xffff0000


@register()
class INX_H(Instruction):
    opcode = 0x23

    def _run(self):
        # note: No condiditon pairs are affected
        value = ((self.state.h << 8) | self.state.l) + 1
        self.state.h = value >> 8
        self.state.l = value & 0xff


@register()
class DAD_H(Instruction):
    opcode = 0x29

    def _run(self):
        hl = ((self.state.h << 8) | self.state.l)
        result = hl + hl

        self.state.h = result >> 8
        self.state.l = result & 0xff
        self.state.condition_codes.cy = result > 0xffff0000


class ADD_B(ArithmeticInstruction):
    opcode = 0x80

    def _run(self):
        return self.state.a + self.state.b


class ADD_C(ArithmeticInstruction):
    opcode = 0x81

    def _run(self):
        return self.state.a + self.state.c


class ADD_M(ArithmeticInstruction):
    opcode = b'\0x86'

    def _run(self, state):
        offset = state.h << 8 | state.l
        return state.a + state.memory[state.pc + 1] + state.memory[offset]


class ADD_I(ArithmeticInstruction):
    opcode = b'\xc6'

    def _run(self, state):
        return state.a + state.memory[state.pc + 1]
