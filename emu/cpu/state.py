class Singelton(object):
    def __new__(cls, *p, **k):
        if '_the_instance' not in cls.__dict__:
            cls._the_instance = object.__new__(cls)
        return cls._the_instance


class ConditionCodes(Singelton):
    z = 0
    s = 0
    p = 0
    cy = 0
    ac = 0
    pad = 0


class State8080(Singelton):
    def __init__(self):
        self.a = 0
        self.b = 0
        self.c = 0
        self.d = 0
        self.e = 0
        self.h = 0
        self.l = 0
        self.sp = 0
        self.pc = 0
        self.memory = [0] * 0xffff
        self.condition_codes = ConditionCodes()
        self.interrupt_enabled = False

    def get_opcode(self, offset=0):
        return self.memory[self.pc + offset]

    def get_from_stack(self, offset=0):
        return self.memory[self.sp + offset]

    def get_addr_from_opcodes(self):
        return (self.get_opcode(2) << 8) | self.get_opcode(1)
